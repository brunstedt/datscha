import React, { useState } from 'react';
import './App.css';
import Login from './components/Login/Login';
import PropertySelector from './components/PropertySelector/PropertySelector'

function App() {
  const [auth, setAuth] = useState({ token: null, error: false });
  
  const storeToken = (authResponse) => {
    if(authResponse && authResponse.token) {
      setAuth({...auth, token: authResponse.token, error: false});
    }else{
      setAuth({...auth, error: 'Could not obtain token'});
    }
  }

  return (
    <div className="App">
      <header className="App-header">
        <h1 className="App-title">Datscha <span className="App-title--sub"> | code test</span></h1>
      </header>
      <main>
        {!auth.token && 
          <Login onLogin={storeToken} />
        }

        {auth.token &&
          <PropertySelector token={auth.token} />
        }

      </main>
    </div>
  );
}

export default App;
