import React, { useState, useEffect } from 'react';
import './PropertySelector.css';
import CONSTANTS from '../../constants';

const PropertySelector = ({ token }) => {
    const [componentState, setComponentState] = useState({ error: false, isLoading: false });
    const [propertyData, setPropertyData] = useState();

    const [selectedFilters, setSelectedFilters] = useState({ area: '', premises: '', propertyRent: '' });
    const [filterData, setFilterData] = useState({
        intervals: [
            '0-500',
            '501-1000',
            '1001-2000',
            '2001-5000'
        ],
        premises: [],
        properties: []
    });

    useEffect(() => {
        if (token) {
            fetchData()
                .then(data => {
                    setPropertyData(data);
                    setComponentState({ ...componentState, isLoading: false, error: false });
                })
        } else {
            console.error('Missing token')
        }
    }, [])

    useEffect(() => {
        setFilterData({ ...filterData, ...generateFilterData() });
    }, [selectedFilters])


    const generateFilterData = () => {
        // We need a selected 'area' to start filtering.
        if (selectedFilters.area === '') return false

        const filteredByArea = filterByArea(selectedFilters.area);
        const properties = filteredByArea.filter(p => p.type === selectedFilters.premises);

        // Get unique values for presentation
        let premises = [];
        filteredByArea.map(e => {
            if (!premises.includes(e.type)) {
                return premises = [...premises, e.type];
            }else{
                return false
            }
        });

        return { premises, properties };
    }

    const filterByArea = (areaFilter) => {
        const [minArea, maxArea] = areaFilter.trim().split('-');
        let filteredCollection = [];
        propertyData.forEach(e => {
            const premises = e.premisesTypes.filter(pt => pt.area <= maxArea && pt.area >= minArea);
            premises.map(p => p.name = e.name); // Add property name to premise
            filteredCollection = [...filteredCollection, ...premises];
        })
        return filteredCollection;
    }

    const handleDropdownChange = (e) => {
        const { name, value } = e.target;
        const propertyRent = name === 'properties' ? value : '';
        let premises = name === 'premises' ? value : selectedFilters.premises;
        premises = name === 'area' ? '' : premises;

        setSelectedFilters({ ...selectedFilters, premises, propertyRent, [name]: value })
    }

    const renderOptions = (type) => {
        switch (type) {
            case 'area':
                return filterData.intervals.map((e, i) => <option key={i} value={e}>{e}</option>)
            case 'premises':
                return filterData.premises.map((e, i) => <option key={i} value={e}>{e}</option>)
            case 'properties':
                return filterData.properties.map((e, i) => <option key={i} value={e.rent}>{e.name} &nbsp; &nbsp; Area: {e.area}</option>)
            default:
                break;
        }
    }

    const renderYearlyRentList = (years = 5) => {
        const rent = parseInt(selectedFilters.propertyRent);
        let year = 1;
        const render = [];
        while (year <= years) {
            render.push(<li key={year}>Year {year}: {calculateRent(rent, year)}</li>)
            year++;
        }
        return render;
    }

    const calculateRent = (rent, year) => {
        switch (year) {
            case 1:
            case 2:
                return `(${rent} x 0.05) x 12 = ${((rent * 0.05) * 12).toFixed(2)}`;
            case 3:
                return `(${rent} x 0.03) x 12 = ${((rent * 0.03) * 12).toFixed(2)}`;
            default:
                return `(${rent} x 0.02) x 12 = ${((rent * 0.02) * 12).toFixed(2)}`;
        }
    }
 
    const fetchData = () => {
        setComponentState({ ...componentState, isLoading: true });

        const request = fetch(CONSTANTS.propertiesDataUrl, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        });

        return request
            .then(handleErrors)
            .then(res => {
                return res.json();
            })
            .catch(errMsg => setComponentState({ ...componentState, error: errMsg, isLoading: false }))
    }

    const handleErrors = (res) => {
        if (!res.ok) {
            throw res.statusText;
        }
        return res;
    }

    return <div>
        {componentState.isLoading &&
            <div className='calculator--loading'>Loading data...</div>
        }

        {!componentState.isLoading &&
            <div>
                <div className='calculator--column'>
                    <h4 className='calculator--title'>Area</h4>
                    <select name="area" onChange={handleDropdownChange} value={selectedFilters.area}>
                        {selectedFilters.area === '' &&
                            <option value={null}>Select area</option>
                        }
                        {renderOptions('area')}
                    </select>
                </div>
                <div className='calculator--column'>
                    <h4 className='calculator--title'>Premises types</h4>
                    <select name="premises" onChange={handleDropdownChange} value={selectedFilters.premises} disabled={!selectedFilters.area}>
                        {selectedFilters.premises === '' &&
                            <option value={null}>Select premise</option>
                        }
                        {renderOptions('premises')}
                    </select>
                </div>
                <div className='calculator--column'>
                    <h4 className='calculator--title'>Available properties</h4>
                    <select name="properties" onChange={handleDropdownChange} value={selectedFilters.propertyRent} disabled={!selectedFilters.area || !selectedFilters.premises}>
                        {selectedFilters.propertyRent === '' &&
                            <option value={null}>Select property</option>
                        }
                        {renderOptions('properties')}
                    </select>
                </div>
            </div>
        }

        {selectedFilters.propertyRent !== '' &&
            <div className='calculator--rent-wrapper'>
                <h4 className='calculator--title'>Yearly rent</h4>
                <ul className='calculator--rent-list'>
                    {renderYearlyRentList(5)}
                </ul>
            </div>
        }


    </div>
}

export default PropertySelector;