import React from 'react';
import { render } from '@testing-library/react';
import Login from './Login';

test('Renders login form fields', () => {
    const { getByPlaceholderText, getByTestId } = render(<Login />);

    const userNameField = getByTestId('input_username');
    expect(userNameField).toBeInTheDocument();

    const passwordField = getByTestId('input_password');
    expect(passwordField).toBeInTheDocument();
    
    const submitButton = getByTestId('input_submit');
    expect(submitButton).toBeInTheDocument();
});
