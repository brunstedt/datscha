import React, { useState, useEffect } from 'react';
import './Login.css';
import CONSTANTS from '../../constants';

const Login = ({ onLogin }) => {
    const [formState, setFormState] = useState({ isLoading: false, postError: false, isValid: false })
    const [formValues, setFormValues] = useState({ username: '', password: '' });

    useEffect(() => {
        const validation = () => {
            // Very simple validation on input change
            const isValid = formValues.username.length > 0 && formValues.password.length > 0;
            setFormState({ ...formState, isValid });
        }
        validation()
    }, [formValues])

    const handleSubmit = (e) => {
        e.preventDefault();
        if (formState.isValid) {
            setFormState({ ...formState, postError: false, isLoading: true });

            // Pass the token to parent
            postData().then(onLogin);
        } else {
            setFormState({ ...formState, isLoading: false, postError: 'Please enter username and password before submitting' })
        }
    }

    const handleErrors = (res) => {
        if (!res.ok) {
            throw res.statusText;
        }
        return res;
    }

    const postData = () => {
        const request = fetch(CONSTANTS.loginUrl, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ ...formValues })
        });

        return request
            .then(handleErrors)
            .then(res => {
                setFormState({ ...formState, postError: false, isLoading: false });
                setFormValues({ ...formValues, username: '', password: '' });
                return res.json();
            })
            .catch(errMsg => setFormState({ ...formState, postError: errMsg }))
    }

    const handleChange = (e) => {
        const { value, name } = e.target;
        switch (name) {
            case 'username':
                return setFormValues({ ...formValues, username: value.trim() });
            case 'password':
                return setFormValues({ ...formValues, password: value });
            default:
                return false;
        }
    }

    return (
        <div className='login--wrapper'>
            <h3 className='login--title'>Login to proceed</h3>
            <form className='login--form' onSubmit={handleSubmit}>
                <div className='login--form--section'>
                    <label htmlFor="form_login__username">Username</label>
                    <input type="text" id="form_login__username" onChange={handleChange} value={formValues.username} placeholder="Username..." name="username" data-testid="input_username" />
                </div>
                <div className='login--form--section'>
                    <label htmlFor="form_login__password">Password</label>
                    <input type="password" id="form_login__password" onChange={handleChange} value={formValues.password} placeholder="Password..." name="password" data-testid="input_password" />
                </div>

                <input type="submit" disabled={!formState.isValid && !formState.isLoading} value="Log in" data-testid="input_submit" />

                {formState.postError &&
                    // Error message
                    <div className='login--form--error'>
                        <span className='login--form--error--title'>There was an error logging in:</span>
                        {formState.postError}
                    </div>
                }
            </form>
        </div>
    )
}

export default Login;