export default {
    loginUrl: 'https://datscha-fe-code-test-api.azurewebsites.net/login',
    propertiesDataUrl: 'https://datscha-fe-code-test-api.azurewebsites.net/properties'
}